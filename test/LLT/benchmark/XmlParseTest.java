/*
 * Copyright 2024 Primeton Information Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * TODO 此处填写 class 信息
 *
 * @author wangwb (mailto:wangwb@primeton.com)
 */

public class XmlParseTest {

    private static int WARMUP = 10000;

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        String xml = xml();
        for (int i = 0; i < WARMUP; i++) {
            S3XmlElement.fromXml(xml);
        }

        long beginTime = System.nanoTime();
        for (int i = 0; i < 100; i++) {
            S3XmlElement.fromXml(xml);
        }
        System.out.println("===== XmlParse: " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - beginTime) + "ms");
    }

    private static String xml() {
        String contents = "";
        for (int i = 0; i < 100; i++) {
            // @formatter:off
            contents += "<Contents>\n" 
                    + "        <Key>mock_key_"+i+"</Key>\n"
                    + "        <LastModified>2024-03-22T08:04:59.000Z</LastModified>\n"
                    + "        <ETag>&quot;1d0f249c316092c34288a77d779eeaba-"+i+"&quot;</ETag>\n"
                    + "        <ChecksumAlgorithm>SHA256</ChecksumAlgorithm>\n"
                    + "        <Size>5</Size>\n"
                    + "        <StorageClass>STANDARD</StorageClass>\n"
                    + "    </Contents>";
            // @formatter:on
        }
        // @formatter:off
        return "<ListBucketResult\n"
                + "    xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">\n"
                + "    <Name>cj-test11</Name>\n"
                + "    <Prefix></Prefix>\n"
                + "    <KeyCount>100</KeyCount>\n"
                + "    <MaxKeys>100</MaxKeys>\n"
                + "    <IsTruncated>false</IsTruncated>\n"
                + contents.toString()
                + "</ListBucketResult>";
        // @formatter:on
    }

    public static class S3XmlElement {
        public String name;
        public String content;
        private Map<String, String> attrMap = new HashMap<String, String>();
        private List<S3XmlElement> childs = new ArrayList<S3XmlElement>();
        private S3XmlElement parent;

        public static S3XmlElement fromXml(String xml) {
            S3XmlElement root = new S3XmlElement();
            S3XmlElement current = root;

            int flag = 0; // 1: start element; 2: start attr; 3: end attr; 4: closing element; 5: end element
            int textBegin = 0;
            String attrName = "";
            String attrValue = "";
            int quotes = 0; // 1: single; 2: double 
            int pos = 0;
            int len = xml.length();
            while (pos < len) {
                char rune = xml.charAt(pos);
                pos++;
                if (rune == '<') {
                    if (xml.charAt(pos) != '/') {
                        S3XmlElement child = new S3XmlElement();
                        current.addChild(child);
                        current = child;
                        flag = 1;
                        textBegin = pos;
                    } else {
                        // 结束节点
                        current.content = xml.substring(textBegin, pos - 1);
                        current = current.parent;
                        flag = 0;
                    }
                } else if (rune == ' ') {
                    if (flag == 1) {
                        // 节点名称
                        current.name = xml.substring(textBegin, pos - 2);
                        textBegin = pos;
                        flag = 2;
                    }
                } else if (rune == '=') {
                    if (flag == 2) {
                        if (quotes == 0) {
                            char next = xml.charAt(pos);
                            if (next == '\'') {
                                quotes = 1;
                                attrName = xml.substring(textBegin, pos - 1);
                                textBegin = pos;
                                pos++;
                            } else if (next == '"') {
                                quotes = 2;
                                attrName = xml.substring(textBegin, pos - 1);
                                textBegin = pos;
                                pos++;
                            }
                        }
                    }
                } else if (rune == '\'') {
                    if (quotes == 1) {
                        attrValue = xml.substring(textBegin, pos);
                        textBegin = pos;
                        current.attrMap.put(attrName, attrValue);
                    }
                } else if (rune == '"') {
                    if (quotes == 2) {
                        attrValue = xml.substring(textBegin, pos);
                        textBegin = pos;
                        current.attrMap.put(attrName, attrValue);
                    }
                } else if (rune == '/') {
                    if (xml.charAt(pos) == '>') {
                        // 结束节点
                        // 类似于 "<test/>"
                        if (current.name == "") {
                            current.name = xml.substring(textBegin, pos - 2);
                        }
                        current = current.parent;
                        flag = 0;
                        textBegin = pos;
                    }
                } else if (rune == '>') {
                    if (flag == 1) {
                        current.name = xml.substring(textBegin, pos - 1);
                    }
                    textBegin = pos;
                    flag = 4;
                }

            }
            return root.childs.get(0);
        }

        private void addChild(S3XmlElement child) {
            childs.add(child);
            child.parent = this;
        }

        public String toString() {
            String childsXml = childs.stream().map(S3XmlElement::toString).collect(Collectors.joining(""));
            String attrsXml = attrMap.entrySet().stream().map(entry -> entry.getKey() + "=" + entry.getValue()).collect(Collectors.joining(" "));
            return "<" + name + (attrsXml.equals("") ? "" : attrsXml) + ">" + content + childsXml + "</" + name + ">";
        }
    }

}

/*
 * 修改历史
 * $Log$
 */