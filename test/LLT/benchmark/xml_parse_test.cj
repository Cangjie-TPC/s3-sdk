/*
 * Copyright 2024 Primeton Information Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package benchmark

import std.sync.sleep
import std.collection.{Map, HashMap, ArrayList, LinkedList, forEach, collectString, collectArrayList, map}
import std.time.{DateTime, Duration, DurationExtension, TimeZone}
import encoding.xml.{XmlParser, SaxHandler, XmlElement, XmlAttr}
import s3client.core.*
import s3client.util.*
import std.unittest.*
import std.unittest.testmacro.*
/*
 *
 * @author: wangwb
 * @date: 2024-04-25
 */
@Test
@Configure[minDuration: Duration.second * 5, measurement: TimeNow(TimeUnit.Millis)]
@Configure[warmup: Duration.second]
public class XmlParseTest {
    let xml1 = xml1()
    let xml2 = xml2()

    @TestCase
    @Bench
    public func testS3XmlElement1_xml1(): Unit {
        for (_ in 0..100) {
            let ele = S3XmlElement.fromXml(xml1, parser: S3XmlElement.PARSER_SAX)
        }
    }

    @TestCase
    @Bench
    public func testS3XmlElement2_xml1(): Unit {
        for (_ in 0..100) {
            let ele = S3XmlElement.fromXml(xml1, parser: S3XmlElement.PARSER_SIMPLE)
        }
    }

    @TestCase
    @Bench
    public func testStdDomParse_xml1(): Unit {
        for (_ in 0..100) {
            let ele = XmlParser().parse(xml1).getOrThrow()
        }
    }

    @TestCase
    @Bench
    public func testStdSaxParse_xml1(): Unit {
        for (_ in 0..100) {
            XmlParser(NoneHandler()).parse(xml1)
        }
    }

    @TestCase
    @Bench
    public func testS3XmlElement1_xml2(): Unit {
        for (_ in 0..100) {
            let ele = S3XmlElement.fromXml(xml2, parser: S3XmlElement.PARSER_SAX)
        }
    }

    @TestCase
    @Bench
    public func testS3XmlElement2_xml2(): Unit {
        for (_ in 0..100) {
            let ele = S3XmlElement.fromXml(xml2, parser: S3XmlElement.PARSER_SIMPLE)
        }
    }

    @TestCase
    @Bench
    public func testStdDomParse_xml2(): Unit {
        for (_ in 0..100) {
            let ele = XmlParser().parse(xml2).getOrThrow()
        }
    }

    @TestCase
    @Bench
    public func testStdSaxParse_xml2(): Unit {
        for (_ in 0..100) {
            XmlParser(NoneHandler()).parse(xml2)
        }
    }
}

class NoneHandler <: SaxHandler {
    public func startDocument(): Unit {
    }
    public func endDocument(): Unit {
    }
    public func startElement(_: String, _: ArrayList<XmlAttr>): Unit {
    }
    public func endElement(_: String): Unit {
    }
    public func characters(_: String): Unit {
    }
}

func xml1(): String {
    let contents = StringBuilder();
    for (i in 0..100) {
        contents.append(
            """
<Contents>
    <Key>mock_key_${i}</Key>
    <LastModified>2024-03-22T08:04:59.000Z</LastModified>
    <ETag>&quot;1d0f249c316092c34288a77d779eeaba-${i}&quot;</ETag>
    <ChecksumAlgorithm>SHA256</ChecksumAlgorithm>
    <Size>5</Size>
    <StorageClass>STANDARD</StorageClass>
</Contents>
"""
        )
    }
    return """
<ListBucketResult
    xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
    <Name>cj-test11</Name>
    <Prefix></Prefix>
    <KeyCount>100</KeyCount>
    <MaxKeys>100</MaxKeys>
    <IsTruncated>false</IsTruncated>
    ${contents}
</ListBucketResult>
"""
}

func xml2(): String {
    return """
<?xml version="1.0" encoding="UTF-8"?>
<AccessControlPolicy>
   <Owner>
      <DisplayName>DisplayName</DisplayName>
      <ID>ID</ID>
   </Owner>
   <AccessControlList>
      <Grant>
         <Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xsi:type">
            <DisplayName>DisplayName</DisplayName>
            <EmailAddress>EmailAddress</EmailAddress>
            <ID>ID</ID>
            <xsi:type>xsi:type</xsi:type>
            <URI>URI</URI>
         </Grantee>
         <Permission>Permission</Permission>
      </Grant>
   </AccessControlList>
</AccessControlPolicy>
"""
}
