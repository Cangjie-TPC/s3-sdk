/*
 * Copyright 2024 Primeton Information Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import std.unittest.*
import std.unittest.testmacro.*

/*
 *
 * @author: wangwb
 * @date: 2024-02-05
 */

// DEPENDENCE: _config.cj _config.properties
// EXEC: cjc %import-path %L %l %f _config.cj _config.properties
// EXEC: ./main
main(): Int64 {
    let tester = WaiterTest()
    let report = tester.asTestSuite().runTests()
    report.reportTo(ConsoleReporter())
    0
}

@Test
public class WaiterTest {
    @TestCase
    public func test1(): Unit {
        println("======= waitUntilBucketExists")
        let begin = DateTime.nowUTC()
        let rsp1 = s3.waitUntilBucketExists(HeadBucketRequest(bucket: "bucket-2024"), Duration.second * 3)
        println(rsp1)
        @Expect(rsp1, false)
        println(Duration.since(begin))

        println("======= waitUntilBucketNotExists")
        let rsp2 = s3.waitUntilBucketNotExists(HeadBucketRequest(bucket: "bucket-2024"), Duration.second * 3)
        println(rsp2)
        @Expect(rsp2, true)

        println("======= waitUntilObjectExists")
        let rsp3 = s3.waitUntilObjectExists(
            HeadObjectRequest(bucket: "bucket-2024", key: "key-01"),
            Duration.second * 3
        )
        println(rsp3)
        @Expect(rsp3, false)

        println("======= waitUntilObjectNotExists")
        let rsp4 = s3.waitUntilObjectNotExists(
            HeadObjectRequest(bucket: "bucket-2024", key: "key-01"),
            Duration.second * 3
        )
        println(rsp4)
        @Expect(rsp4, true)
    }
}
